Daily-Scrumboard
================

Dies ist ein Scrumboard, das den Schwerpunkt auf die Durchführung eines Daily-Standups eines Entwicklerteams legt.

Im Gegensatz zu anderen Scrumboard-Lösungen zeigt es nicht nur den aktuellen Status der Stories an, sondern auch das, 
was am Board passiert ist.  
So wird sehr schnell klar, welche Themen für die Historie interessant sind ()"hier gibt es eine Aufgabe, die hängt", "es 
ist eine neue Aufgabe hinzugekommen" usw.).

Hinweis: Die Anwendung wurde ausschließlich in Google Chrome Version 80.0 getestet.

## Installation

1. Gehe ins Verzeichnis `server` und starte die Spring-Boot-Anwendung (`mvn package && java -jar target/scrumboard*jar`)
2. Konfiguriere die H2-Datenbank in `server/src/main/resources/application.properties`
3. Konfiguriere die Kerndaten in `server/src/main/resources/data.sql`
4. Konfiguriere die URL des Servers in der Datei `config.js`
5. Gehe ins Verszeichnis `client` und führe `npm install` aus 
6. Starte die Client-Anwendung mit `npm run serve`
7. Öffne die Anwendung unter der ausgegebenen URL

## Entwicklung 

### Client 

Für die Entwicklung des Clients wird [node.js](https://nodejs.org) benötigt.

Nach der Installation muss vue/cli global im System installiert werden: `npm install -g @vue/cli`.

Wie man dann den Server startet etc. kann der `client/README.md` entnommen werden.

#### Project setup
```
npm install
```

##### Compiles and hot-reloads for development
```
npm run serve
```

##### Compiles and minifies for production
```
npm run build
```

##### Lints and fixes files
```
npm run lint
```

##### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


#### Architektur

* Abweichend von 
[der weit verbreiteten Ordnungstruktur von Vue-Projekten](https://itnext.io/how-to-structure-a-vue-js-project-29e4ddc1aeeb) 
wird hier [Atomic Design](https://bradfrost.com/blog/post/atomic-web-design/) verfolgt.  
* Wenn zu einer Vue-Datei weitere Dateien gehören, befinden diese sich in einem Ordner mit gleichem Namen (`Daily.vue` &rarr; `historie`) ).  
* Um die Vue-Templates nicht zu groß werden zu lassen gibt es Hilfsobjekte in den Ordnern `helpers`.

Im Bereich `dataaccess` sind die Gateways zum Server definiert.

Im Bereich `model` befindet sich das globale Datenmodell. Es vermischt das zu persistierende Datenmodell ("Events") mit 
dem, womit in der UI gearbeitet werden soll ("Board", "Story", "Task"). Ob das so schlau ist ...?!

Ansonsten wird mit [Event-Sourcing](https://de.wikipedia.org/wiki/Event_Sourcing) gearbeitet. Der wichtigste Helper 
hier ist die Controller-Klasse `EventPlayer`, die aus den bekannten Events das Datenmodell der UI zu einer bestimmten
 Zeit erstellt.  
 Um Beziehungen zwischen den Datenmodellen herzustellen existiert die `instanceId`, die alle Objekte haben, die von 
 der Klasse `ModelWithInstanceId` ableiten.
 
 TODO die instanceId wird zur Zeit clientseitig generiert (wie überhaupt alle Testdaten). Das ist natürlich :cheese:.

### Server

Der Server benötigt eine Maven Installation (getestet mit 3.5.0) und ein Java JDK (getestet mit Open JDK 13).



