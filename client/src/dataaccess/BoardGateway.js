import Board from './../model/board/Board'
import CONFIG from "./../config";
import HttpJsonRequest from "./HttpJsonRequest";
import ErrorHandler from './ErrorHandler'

export default class BoardGateway {

  constructor() {
    this._errorHandler = new ErrorHandler();
  }

  // noinspection JSMethodCanBeStatic
  getBoard(callback) {

    const URL = CONFIG.BASE_URI + '/boards';
    new HttpJsonRequest(URL, (status, boards) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
      else callback(Board.fromJSON(boards[0]));
    }).get();
  }
}