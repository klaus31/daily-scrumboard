import CONFIG from "./../config";
import HttpJsonRequest from "./HttpJsonRequest";
import ErrorHandler from './ErrorHandler'

export default class ComingupGateway {

  constructor() {
    this._errorHandler = new ErrorHandler();
  }

  // noinspection JSMethodCanBeStatic
  getAll(callback) {

    const URL = `${CONFIG.BASE_URI}/comingup`;
    new HttpJsonRequest(URL, (status, json) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
      else callback(json);
    }).get();
  }

  post(names) {
    const URL = `${CONFIG.BASE_URI}/comingup`;
    new HttpJsonRequest(URL, (status) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
    }).post(names);
  }
}