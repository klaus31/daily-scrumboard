import GLOBAL_MESSAGE_CTRL from "./../templates/messagebox/helpers/GlobalMessageCtrl";
import Message from "../templates/messagebox/helpers/Message";

export default class ErrorHandler {

  constructor() {

  }

  handle(text) {
    const message = Message.createErrorShownUntilClosed(text);
    GLOBAL_MESSAGE_CTRL.showMessage(message);
  }

  no200(status, url) {
    this.handle(`Verbindungsfehler ${status} @ ${url}`)
  }
}