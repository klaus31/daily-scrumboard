import CONFIG from "./../config";
import HttpJsonRequest from "./HttpJsonRequest";
import BoardEvent from "../model/events/BoardEvent";
import ErrorHandler from './ErrorHandler'

export default class EventsGateway {

  constructor() {
    this._errorHandler = new ErrorHandler();
  }

  postEvent(event, callback) {
    callback = callback || function(){};
    const URL = CONFIG.BASE_URI + '/events';
    new HttpJsonRequest(URL, (status, json) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
      else callback(json.currentInstanceId);
    }).post(event.toJSON());
  }

  // noinspection JSMethodCanBeStatic
  getEvents(board, callback) {

    const URL = CONFIG.BASE_URI + '/events';
    new HttpJsonRequest(URL, (status, json) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
      const events = json.map(event => BoardEvent.fromJSON(event, board.teammembers));
      callback(events);
    }).get();

  }

  deleteAll(callback) {
    const URL = CONFIG.BASE_URI + '/events';
    new HttpJsonRequest(URL, callback).delete();
  }

}