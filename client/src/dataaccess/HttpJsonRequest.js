export default class HttpJsonRequest {

  constructor(url, callback) {
    this._url = url;
    this._shouldBeAsync = true;
    this._request = new XMLHttpRequest();
    const me = this;
    this._request.onload = function () {
      let status = me._request.status;
      let data = me._request.responseText;
      if (data) data = JSON.parse(data);
      callback(status, data);
    };
  }

  get() {
    this._request.open('GET', this._url, this._shouldBeAsync);
    this._request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    this._request.send();
  }

  uploadFile(data) {
    var formData = new FormData();
    formData.append("file", data);
    this._request.open('POST', this._url);
    this._request.send(formData);
  }

  post(json) {
    this._request.open('POST', this._url, this._shouldBeAsync);
    this._request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    this._request.send(json ? JSON.stringify(json) : '');
  }

  delete() {
    this._request.open('DELETE', this._url, this._shouldBeAsync);
    this._request.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    this._request.send();
  }
}