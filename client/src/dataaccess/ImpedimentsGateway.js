import CONFIG from "./../config";
import HttpJsonRequest from "./HttpJsonRequest";
import ErrorHandler from './ErrorHandler'
import Impediment from "../model/Impediment";

export default class ImpedimentsGateway {

  constructor() {
    this._errorHandler = new ErrorHandler();
  }

  // noinspection JSMethodCanBeStatic
  getImpediments(callback) {

    const URL = CONFIG.BASE_URI + '/documents/impediments';
    new HttpJsonRequest(URL, (status, document) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
      else {
        let result = [];
        if (!document) callback(result);
        else {
          for (let i = 0; i < document.json.impediments.length; i++) {
            result.push(Impediment.fromJSON(document.json.impediments[i]));
          }
          callback(result);
        }
      }
    }).get();
  }

  persist(impediments) {
    const URL = CONFIG.BASE_URI + '/documents';
    const document = {
      key: 'impediments',
      json: {
        impediments: impediments
      }
    };
    new HttpJsonRequest(URL, (status) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
    }).post(document);
  }
}