import CONFIG from "./../config";
import HttpJsonRequest from "./HttpJsonRequest";
import ErrorHandler from './ErrorHandler'
import Policy from "../model/Policy";

export default class PoliciesGateway {

  constructor() {
    this._errorHandler = new ErrorHandler();
  }

  // noinspection JSMethodCanBeStatic
  getPolicies(callback) {

    const URL = CONFIG.BASE_URI + '/documents/policies';
    new HttpJsonRequest(URL, (status, document) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
      else {
        let result = [];
        if (!document) callback(result);
        else {
          for (let i = 0; i < document.json.policies.length; i++) {
            result.push(Policy.fromJSON(document.json.policies[i]));
          }
          callback(result);
        }
      }
    }).get();
  }

  persist(policies) {
    const URL = CONFIG.BASE_URI + '/documents';
    const document = {
      key: 'policies',
      json: {
        policies: policies
      }
    };
    new HttpJsonRequest(URL, (status) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
    }).post(document);
  }
}