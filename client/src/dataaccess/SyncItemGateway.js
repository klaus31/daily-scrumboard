import CONFIG from "./../config";
import HttpJsonRequest from "./HttpJsonRequest";
import SyncItem from "../model/SyncItem";
import ErrorHandler from './ErrorHandler'

export default class SyncItemGateway {

  constructor() {
    this._errorHandler = new ErrorHandler();
  }

  // noinspection JSMethodCanBeStatic
  getSyncItem(board, callback) {
    const URL = CONFIG.BASE_URI + '/syncitem';
    new HttpJsonRequest(URL, (status, json) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
      const syncItem = SyncItem.fromJSON(json, board.teammembers);
      callback(syncItem);
    }).get();
  }

  postDailyInstanceId(daily) {
    const URL = CONFIG.BASE_URI + '/syncitem';
    new HttpJsonRequest(URL, (status) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
    }).post(daily.toJSON());
  }

}