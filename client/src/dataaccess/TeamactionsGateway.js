import CONFIG from "./../config";
import HttpJsonRequest from "./HttpJsonRequest";
import ErrorHandler from './ErrorHandler'
import Teamaction from "../model/Teamaction";

export default class TeamactionsGateway {

  constructor() {
    this._errorHandler = new ErrorHandler();
  }

  // noinspection JSMethodCanBeStatic
  getTeamactions(callback) {

    const URL = CONFIG.BASE_URI + '/documents/teamactions';
    new HttpJsonRequest(URL, (status, document) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
      else {
        let result = [];
        if (!document) callback(result);
        else {
          for (let i = 0; i < document.json.teamactions.length; i++) {
            result.push(Teamaction.fromJSON(document.json.teamactions[i]));
          }
          callback(result);
        }
      }
    }).get();
  }

  persist(teamactions) {
    const URL = CONFIG.BASE_URI + '/documents';
    const document = {
      key: 'teamactions',
      json: {
        teamactions: teamactions
      }
    };
    new HttpJsonRequest(URL, (status) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
    }).post(document);
  }
}