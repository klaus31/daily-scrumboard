import CONFIG from "./../config";
import HttpJsonRequest from "./HttpJsonRequest";
import Teammember from "../model/Teammember";
import ErrorHandler from './ErrorHandler'

export default class UsersGateway {

  constructor() {
    this._errorHandler = new ErrorHandler();
    this._onNewTeammemberActions = [];
  }

  // noinspection JSMethodCanBeStatic
  getAll(callback) {

    const URL = `${CONFIG.BASE_URI}/users`;
    new HttpJsonRequest(URL, (status, json) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
      else callback(json.map(teammember => Teammember.fromJSON(teammember)));
    }).get();
  }

  // noinspection JSMethodCanBeStatic
  postNewMeme(blobFile, user, callback) {

    const URL = `${CONFIG.BASE_URI}/users/${user.key}`;
    new HttpJsonRequest(URL, (status, img) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
      else callback(img);
    }).uploadFile(blobFile);
  }

  postNewTeammember(teammember, callback) {
    const URL = `${CONFIG.BASE_URI}/users`;
    new HttpJsonRequest(URL, (status, teammember) => {
      if (status !== 200) this._errorHandler.no200(status, URL);
      else {
        const tm = Teammember.fromJSON(teammember);
        this._onNewTeammemberActions.forEach(cb => cb(tm));
        callback(tm);
      }
    }).post(teammember.toJSON());
  }

  onNewTeammember(callback) {
    this._onNewTeammemberActions.push(callback);
  }
}