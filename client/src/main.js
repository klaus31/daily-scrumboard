import Vue from 'vue'
import Page from './Page'
import 'bootstrap-css-only'
import moment from 'moment';
import VueClipboard from 'vue-clipboard2'

moment.locale('de');

Vue.use(VueClipboard)
Vue.config.productionTip = false;

Vue.directive('focus', {
  inserted: function (el) {
    el.focus();
  }
});

new Vue({
  render: h => h(Page),
}).$mount('#app');

