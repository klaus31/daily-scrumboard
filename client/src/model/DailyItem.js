export default class DailyItem {

  constructor(currentInstanceId) {
    this._currentInstanceId = currentInstanceId;
  }

  get currentInstanceId() {
    return this._currentInstanceId;
  }

  static fromJSON(json) {
    return new DailyItem(json.currentInstanceId);
  }

  toJSON() {
    return {
      currentInstanceId: this._currentInstanceId
    }
  }
}