export default class Impediment {

  constructor(title, positionOnBoard, person) {
    this._title = title || '';
    this._person = person || '';
    this._positionOnBoard = positionOnBoard || 0;
  }

  get title() {
    return this._title;
  }

  set title(title) {
    this._title = title;
  }

  get person() {
    return this._person;
  }

  set person(person) {
    this._person = person;
  }

  get positionOnBoard() {
    return this._positionOnBoard;
  }

  set positionOnBoard(positionOnBoard) {
    this._positionOnBoard = positionOnBoard;
  }

  static fromJSON(json) {
    return new Impediment(json.title, json.positionOnBoard, json.person);
  }

  toJSON() {
    return {
      title: this._title,
      person: this._person,
      positionOnBoard: this._positionOnBoard
    }
  }

  equals(other) {
    return this.title === other.title && this.person === other.person && this.positionOnBoard === other.positionOnBoard;
  }

  static createEmpty() {
    return new Impediment('', 0, '');
  }
}