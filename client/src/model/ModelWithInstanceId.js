/**
 * Klassen die diese Klasse implementieren, haben eine id (global eindeutig).
 */
export default class ModelWithInstanceId {

  constructor(instanceId) {
    this._instanceId = instanceId;
  }

  get instanceId() {
    return this._instanceId;
  }

  set instanceId(instanceId) {
    if(this._instanceId) throw 'this instance already has an instanceId';
    this._instanceId = instanceId;
  }

  toJSON() {
    return {
      instanceId: this._instanceId
    }
  }

  equals(other) {
    return this.instanceId === other._instanceId;
  }
}