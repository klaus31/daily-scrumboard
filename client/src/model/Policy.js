import moment from "moment";

export default class Policy {

  constructor(title, description, createdAt, validUntil, reason) {
    this._title = title || '';
    this._description = description || '';
    this._createdAt = createdAt ? moment(createdAt) : moment();
    this._validUntil = validUntil ? moment(validUntil) : moment().add(1, 'years');
    this._reason = reason || '';
  }

  get title() {
    return this._title;
  }

  set title(title) {
    this._title = (title || '').trim();
  }

  get description() {
    return this._description;
  }

  set description(description) {
    this._description = (description || '').trim();
  }

  get createdAt() {
    return this._createdAt;
  }

  set createdAt(createdAt) {
    this._createdAt = createdAt ? moment(createdAt) : moment();
  }

  get reason() {
    return this._reason;
  }

  set reason(reason) {
    this._reason = reason;
  }

  get validUntil() {
    return this._validUntil;
  }

  get pretty() {
    const me = this;
    class Pretty {
      get validUntil() {
        return me.validUntil.format('LL');
      }
      get createdAt() {
        return me.createdAt.format('LL');
      }
    }
    return new Pretty();
  }

  set validUntil(validUntil) {
    this._validUntil = validUntil ? moment(validUntil) : moment().add(1, 'years');
  }

  static fromJSON(json) {
    return new Policy(json.title, json.description, json.createdAt, json.validUntil, json.reason);
  }

  toJSON() {
    return {
      title: this._title,
      description: this._description,
      createdAt: this._createdAt.format('YYYY-MM-DD'),
      validUntil: this._validUntil.format('YYYY-MM-DD'),
      reason: this._reason
    }
  }

  get isReadyToSafe() {
    return !!this._title.length;
  }

  static createEmpty() {
    return new Policy();
  }
}
