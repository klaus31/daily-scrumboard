class Settings {
  constructor() {
    this._seperateClicksBySwimlaneInHistorie = false;
    this._seperateClicksBySwimlaneAndTasksInHistorie = false;
    this._showCardElementTicket = true;
    this._showCardElementBranch = false;
    this._showCardElementAssignees = true;
    this._markOrphanTasks = false;
    this._historieShowEventLogs = false;
    this._historieShowTimeline = false;
    this._historieTimelineUpdateOnSlide = false;
    this._showCardInSameSizes = false;
    this._showCardElementPrio = false;
    this._showDeletedCards = false;
    this._showCardElementPrioHighOnly = false;
    if (localStorage.getItem('settings')) {
      let json = JSON.parse(localStorage.getItem('settings'));
      this._seperateClicksBySwimlaneInHistorie = !!json.seperateClicksBySwimlaneInHistorie;
      this._seperateClicksBySwimlaneAndTasksInHistorie = typeof json.seperateClicksBySwimlaneAndTasksInHistorie === 'undefined' ? true : !!json.seperateClicksBySwimlaneAndTasksInHistorie;
      this._markOrphanTasks = !!json.markOrphanTasks;
      this._historieShowEventLogs = !!json.historieShowEventLogs;
      this._historieShowTimeline = !!json.historieShowTimeline;
      this._historieTimelineUpdateOnSlide = !!json.historieTimelineUpdateOnSlide;
      this._showCardElementEffort = !!json.showCardElementEffort;
      this._showCardElementPrio = !!json.showCardElementPrio;
      this._showDeletedCards = !!json.showDeletedCards;
      this._showCardElementPrioHighOnly = !!json.showCardElementPrioHighOnly;
      this._showCardElementBranch = !!json.showCardElementBranch;
      this._showCardElementTicket = typeof json.showCardElementTicket === 'undefined' ? true : !!json.showCardElementTicket;
      this._showCardElementAssignees = typeof json.showCardElementAssignees === 'undefined' ? true : !!json.showCardElementAssignees;
      this._showCardInSameSizes = !!json.showCardInSameSizes;
    }
  }

  get seperateClicksActive() {
    return this._seperateClicksBySwimlaneInHistorie;
  }

  get showDeletedCards() {
    return this._showDeletedCards;
  }

  set showDeletedCards(showDeletedCards) {
    this._showDeletedCards = showDeletedCards;
    this._storeLocal();
  }

  get showSomeCardElementDetails() {
    return this._showCardElementAssignees || this._showCardElementEffort || this._showCardElementTicket || this._showCardElementPrio;
  }

  get seperateClicksBySwimlaneInHistorie() {
    return this._seperateClicksBySwimlaneInHistorie;
  }

  set seperateClicksBySwimlaneInHistorie(seperateClicksBySwimlaneInHistorie) {
    this._seperateClicksBySwimlaneInHistorie = seperateClicksBySwimlaneInHistorie;
    if (!seperateClicksBySwimlaneInHistorie) this._seperateClicksBySwimlaneAndTasksInHistorie = false;
    this._storeLocal();
  }

  get showCardInSameSizes() {
    return this._showCardInSameSizes;
  }

  set showCardInSameSizes(showCardInSameSizes) {
    this._showCardInSameSizes = showCardInSameSizes;
    this._storeLocal();
  }

  get markOrphanTasks() {
    return this._markOrphanTasks;
  }

  set markOrphanTasks(markOrphanTasks) {
    this._markOrphanTasks = markOrphanTasks;
    this._storeLocal();
  }

  get showCardElementEffort() {
    return this._showCardElementEffort;
  }

  set showCardElementEffort(showCardElementEffort) {
    this._showCardElementEffort = showCardElementEffort;
    this._storeLocal();
  }

  get showCardElementPrio() {
    return this._showCardElementPrio;
  }

  set showCardElementPrio(showCardElementPrio) {
    this._showCardElementPrio = showCardElementPrio;
    if (!showCardElementPrio) this._showCardElementPrioHighOnly = false;
    this._storeLocal();
  }

  get showCardElementPrioHighOnly() {
    return this._showCardElementPrioHighOnly;
  }

  set showCardElementPrioHighOnly(showCardElementPrioHighOnly) {
    this._showCardElementPrioHighOnly = showCardElementPrioHighOnly;
    this._storeLocal();
  }


  get seperateClicksBySwimlaneAndTasksInHistorie() {
    return this._seperateClicksBySwimlaneAndTasksInHistorie;
  }

  set seperateClicksBySwimlaneAndTasksInHistorie(seperateClicksBySwimlaneAndTasksInHistorie) {
    this._seperateClicksBySwimlaneAndTasksInHistorie = seperateClicksBySwimlaneAndTasksInHistorie;
    this._storeLocal();
  }

  get showCardElementTicket() {
    return this._showCardElementTicket;
  }

  set showCardElementTicket(showCardElementTicket) {
    this._showCardElementTicket = showCardElementTicket;
    this._storeLocal();
  }

  get showCardElementAssignees() {
    return this._showCardElementAssignees;
  }

  set showCardElementAssignees(showCardElementAssignees) {
    this._showCardElementAssignees = showCardElementAssignees;
    this._storeLocal();
  }

  get showCardElementBranch() {
    return this._showCardElementBranch;
  }

  set showCardElementBranch(showCardElementBranch) {
    this._showCardElementBranch = showCardElementBranch;
    this._storeLocal();
  }

  get historieShowEventLogs() {
    return this._historieShowEventLogs;
  }

  set historieShowEventLogs(historieShowEventLogs) {
    this._historieShowEventLogs = historieShowEventLogs;
    this._storeLocal();
  }

  get historieShowTimeline() {
    return this._historieShowTimeline;
  }

  set historieShowTimeline(historieShowTimeline) {
    this._historieShowTimeline = historieShowTimeline;
    if (!historieShowTimeline) this._historieTimelineUpdateOnSlide = false;
    this._storeLocal();
  }

  get historieTimelineUpdateOnSlide() {
    return this._historieTimelineUpdateOnSlide;
  }

  set historieTimelineUpdateOnSlide(historieTimelineUpdateOnSlide) {
    this._historieTimelineUpdateOnSlide = historieTimelineUpdateOnSlide;
    this._storeLocal();
  }

  _storeLocal() {
    localStorage.setItem('settings', JSON.stringify({
      seperateClicksBySwimlaneInHistorie: this._seperateClicksBySwimlaneInHistorie,
      markOrphanTasks: this._markOrphanTasks,
      showCardElementEffort: this._showCardElementEffort,
      showCardElementPrio: this._showCardElementPrio,
      showCardElementPrioHighOnly: this._showCardElementPrioHighOnly,
      showCardElementTicket: this._showCardElementTicket,
      showCardElementAssignees: this._showCardElementAssignees,
      showDeletedCards: this._showDeletedCards,
      showCardElementBranch: this._showCardElementBranch,
      seperateClicksBySwimlaneAndTasksInHistorie: this._seperateClicksBySwimlaneAndTasksInHistorie,
      historieShowEventLogs: this._historieShowEventLogs,
      historieShowTimeline: this._historieShowTimeline,
      historieTimelineUpdateOnSlide: this._historieTimelineUpdateOnSlide,
      showCardInSameSizes: this._showCardInSameSizes,
    }));
  }
}

// Singleton!
export default new Settings()