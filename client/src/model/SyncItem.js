import DailyItem from "./DailyItem";
import BoardEvent from "./events/BoardEvent";

export default class SyncItem {

  constructor(lastBoardEvents, _daily) {
    this._lastBoardEvents = lastBoardEvents;
    this._daily = _daily;
  }

  get daily() {
    return this._daily;
  }

  get lastBoardEvents() {
    return this._lastBoardEvents;
  }

  static fromJSON(json, teammembers) {
    const lastBoardEvents = json.lastBoardEvents.map(event => BoardEvent.fromJSON(event, teammembers));
    const daily = DailyItem.fromJSON(json.daily);
    return new SyncItem(lastBoardEvents, daily);
  }
}