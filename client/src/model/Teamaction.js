export default class Teamaction {

  constructor(title, positionOnBoard, person, deadline) {
    this._title = title || '';
    this._positionOnBoard = positionOnBoard || 0;
    this._person = person || '';
    this._deadline = deadline || '';
  }

  get title() {
    return this._title;
  }

  set title(title) {
    this._title = title;
  }

  get positionOnBoard() {
    return this._positionOnBoard;
  }

  set positionOnBoard(positionOnBoard) {
    this._positionOnBoard = positionOnBoard;
  }

  get person() {
    return this._person;
  }

  set person(person) {
    this._person = person;
  }

  get deadline() {
    return this._deadline;
  }

  set deadline(deadline) {
    this._deadline = deadline;
  }

  static fromJSON(json) {
    return new Teamaction(json.title, json.positionOnBoard, json.person, json.deadline);
  }

  toJSON() {
    return {
      title: this._title,
      positionOnBoard: this._positionOnBoard,
      person: this._person,
      deadline: this._deadline
    }
  }

  static createEmpty() {
    return new Teamaction('', 0, '', '');
  }

  get isIncomplete() {
    return !this._title.trim() || !this._person.trim() || !this._deadline.trim()
  }

  equals(other) {
    return this.title === other.title && this.person === other.person && this.deadline === other.deadline;
  }
}