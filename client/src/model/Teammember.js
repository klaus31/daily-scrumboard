export default class Teammember {

  /**
   * @param name to show
   * @param picture base64 encoded 200px x 200px
   * @param key never changing for same teammember
   */
  constructor(name, picture, key) {
    this._key = key;
    this._name = name;
    this._picture = picture;
  }

  get name() {
    return this._name;
  }

  get key() {
    return this._key;
  }

  get base64ImgSrc() {
    return 'data:image/png;base64, ' + this._picture;
  }

  set picture(picture) {
    this._picture = picture;
  }

  static fromJSON(json) {
    return new Teammember(json.name, json.picture, json.key);
  }

  toJSON() {
    return {
      key: this._key,
      name: this._name,
      picture: this._picture
    }
  }

}