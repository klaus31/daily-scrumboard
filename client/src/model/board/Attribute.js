import StateOfTask from "./StateOfTask";

export default class Attribute {

  constructor(key, value) {
    this._key = key;
    this._value = value;
  }

  get key() {
    return this._key;
  }


  get prettyKey() {
    switch (this._key) {
      case 'label':
        return 'Titel';
      case 'lockText':
        return 'Sperre-Text';
      case 'ticketNumber':
        return 'Ticket-Nummer';
      case 'description':
        return 'Beschreibung';
      case 'backgroundColor':
        return 'Hintergrundfarbe';
      case 'textColor':
        return 'Textfarbe';
      case 'branch':
        return 'Branchname';
      case 'points':
        return 'Punkte';
      case 'effort':
        return 'Aufwand';
      case 'order':
        return 'Reihenfolge';
      case 'deleted':
        return 'Löschung';
      default:
        return this._key;
    }
  }


  get value() {
    return this._value;
  }

  toJSON() {
    return {
      key: this._key,
      value: this._value
    };
  }

  static fromJSON(json) {
    // XXX Attribute muss hier sämtliche Verwendungen wissen :see_no_evil:
    // Lösung: In der Verwendung und Speicherung daruf nur der Key verwendet werden.
    if (json.key === 'state') {
      return new Attribute(json.key, StateOfTask.fromJSON(json.value));
    } else {
      return new Attribute(json.key, json.value);
    }
  }

}