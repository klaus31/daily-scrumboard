import StateOfTask from "./StateOfTask";
import Teammember from "../Teammember";

export default class Board {

  constructor(statesOfTask, teammembers) {
    this._statesOfTask = statesOfTask;
    this._teammembers = teammembers;
  }

  get statesOfTask() {
    return this._statesOfTask;
  }

  get firstStateOfTask() {
    return this._statesOfTask[0];
  }

  get lastStateOfTask() {
    return this._statesOfTask[this._statesOfTask.length - 1];
  }

  get teammembers() {
    return this._teammembers;
  }

  addTeammember(teammember) {
    this._teammembers.push(teammember);
  }

  toJSON() {
    const teammembers = [];
    this._teammembers.forEach(teammember => teammembers.push(teammember.toJSON()));
    const possibleStates = [];
    this._statesOfTask.forEach(stateOfTask => possibleStates.push(stateOfTask.toJSON()));
    return {
      teammembers: teammembers,
      possibleStates: possibleStates,
    };
  }

  static fromJSON(json) {
    const statesOfTasks = [];
    json.possibleStates.forEach(possibleState => statesOfTasks.push(StateOfTask.fromJSON(possibleState)));
    const teammembers = [];
    json.teammembers.forEach(teammember => teammembers.push(Teammember.fromJSON(teammember)));
    return new Board(statesOfTasks, teammembers);
  }
}