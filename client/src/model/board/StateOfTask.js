export default class StateOfTask {

  /**
   * @param key immer gleich bleibender Schlüssel, der das spätere Umbenennen einer Spalte ermöglicht,
   * ohne dass die vorhandenen Daten migriert werden müssen.
   * Hat darüber hinaus aber keinerlei Funktion.
   * @param name In der UI letztendlich der Spaltenname
   */
  constructor(key, name) {
    this._key = key;
    this._name = name;
  }

  get name() {
    return this._name;
  }

  equals(other) {
    return this._key === other._key;
  }

  /**
   * Workaround for indexOf with different instances.
   *
   * If possible states are a, b, c, d ...
   * ... and this is a, return 0
   * ... and this is b, return 1
   * ... and this is x, return -1
   */
  calcPositionIn(possibleStates) {
    if(!possibleStates.length) return -1;
    let index = 0;
    while(index < possibleStates.length) {
      if(possibleStates[index].equals(this)) return index;
      index++;
    }
    return -1;
  }

  toJSON() {
    return {
      key: this._key,
      name: this._name,
    };
  }

  static fromJSON(json) {
    return new StateOfTask(json.key, json.name);
  }

}
