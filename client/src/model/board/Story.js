import ModelWithInstanceId from '../ModelWithInstanceId'

export default class Story extends ModelWithInstanceId {

  /**
   * @param instanceId die instanceId des Events, das diese Story erzeugt hat!
   * @param label der Teammember Story
   */
  constructor(instanceId, label) {
    super(instanceId);
    this._label = label;
    this._textColor = null;
    this._backgroundColor = null;
    this._description = null;
    this._points = null;
    this._tasks = [];
    this._active = false;
    this._branch = null;
    this._order = 0;
    this._deleted = false;
  }

  get branch() {
    return this._branch;
  }

  get label() {
    return this._label;
  }

  get textColor() {
    return this._textColor || '#000000';
  }

  get backgroundColor() {
    return this._backgroundColor || '#D8FFE2';
  }

  get tasks() {
    return this._tasks;
  }

  get description() {
    return this._description;
  }

  get active() {
    return this._active;
  }

  set active(active) {
    this._active = active;
  }

  get points() {
    return this._points;
  }

  get order() {
    return this._order;
  }

  get deleted() {
    return this._deleted;
  }

  removeTask(task) {
    this._tasks.splice(this._tasks.indexOf(task), 1);
  }

  getLabelCut(length) {
    if (this.label.length <= length) return this.label;
    else return this.label.substr(0, length - 4) + ' ...';
  }

  updateAttribute(attribute) {
    this['_' + attribute.key] = attribute.value;
  }

  removeDeletedTasks() {
    this._tasks = this._tasks.filter(task => !task.deleted);
  }

  getTasksWithState(stateOfTask) {
    return this._tasks.filter(task => task.hasState(stateOfTask));
  }

  addTask(task) {
    this._tasks.push(task);
  }

  containsTask(instanceId) {
    return this._tasks.some(task => task.instanceId === instanceId);
  }

  searchTask(instanceId) {
    const result = this._tasks.filter(task => task.instanceId === instanceId);
    if (!result.length) throw `Task mit der instanceId ${instanceId} nicht vorhanden`;
    return result[0];
  }

}