import ModelWithInstanceId from '../ModelWithInstanceId'

export default class Task extends ModelWithInstanceId {

  /**
   * @param instanceId die instanceId des Events, das diesen Task erzeugt hat!
   * @param label beschreibt, was zu tun ist
   * @param state this task is in
   * @param story this task is assigned to
   */
  constructor(instanceId, label, state, story) {
    super(instanceId);
    this._assignees = [];
    this._label = label;
    this._textColor = null;
    this._backgroundColor = null;
    this._description = null;
    this._ticketNumber = null;
    this._branch = null;
    this._active = false;
    this._effort = null;
    this._prio = 2;
    this._lockText = '';
    this._state = state;
    this._story = story;
    this._deleted = false;
  }

  matchesSearch(search) {
    if (!search) return true;
    else {
      const searchCopy = search.toLowerCase();
      if (this._assignees.map(assignee => assignee.name.toLowerCase()).some(name => name.indexOf(searchCopy) >= 0)) {
        return true;
      }
      if (this._label.toLowerCase().indexOf(searchCopy) >= 0) {
        return true;
      }
      if (this._description && this._description.toLowerCase().indexOf(searchCopy) >= 0) {
        return true;
      }
      if (this._ticketNumber && this._ticketNumber.indexOf(searchCopy) >= 0) {
        return true;
      }
      if (this._branch && this._branch.toLowerCase().indexOf(searchCopy) >= 0) {
        return true;
      }
      if (this._lockText && this._lockText.toLowerCase().indexOf(searchCopy) >= 0) {
        return true;
      }
    }
    return false;
  }

  get branch() {
    return this._branch;
  }

  get pretty() {
    return {
      effort: ['XS', 'S', 'M', 'L', 'XL'][this.effort - 1],
      prio: ['⬇️', '➡️', '⬆️', '🆘'][this.prio - 1],
      prioLong: ['⬇️ niedrig', '➡️ normal', '⬆️ hoch', '🆘  BLOCKER'][this.prio - 1]
    }
  }

  get isHighPrio() {
    return this.prio >= 3;
  }

  get story() {
    return this._story;
  }

  set story(story) {
    this._story.removeTask(this);
    this._story = story;
    this._story.addTask(this);
  }

  get label() {
    return this._label;
  }

  get textColor() {
    return this._textColor || '#000000';
  }

  get backgroundColor() {
    return this._backgroundColor || '#FFFFE0';
  }

  get description() {
    return this._description;
  }

  get ticketNumber() {
    return this._ticketNumber;
  }

  get active() {
    return this._active;
  }

  set active(active) {
    this._active = active;
  }

  hasHigherPrioThan(other) {
    return parseInt(this._prio) > parseInt(other.prio);
  }

  get lockText() {
    return this._lockText;
  }

  get locked() {
    return !!this._lockText.trim();
  }

  get assignees() {
    return this._assignees;
  }

  get state() {
    return this._state;
  }

  set state(state) {
    this._state = state;
  }

  get effort() {
    return this._effort || 3;
  }

  get prio() {
    return this._prio || 2;
  }

  set prio(prio) {
    this._prio = prio;
  }

  get deleted() {
    return this._deleted;
  }

  hasState(state) {
    return this._state.equals(state);
  }

  addAssignee(assignee) {
    if (!assignee) throw 'missing assignee';
    if (this._assignees.indexOf(assignee) < 0) this._assignees.push(assignee);
  }

  removeAssignee(assignee) {
    if (!assignee) throw 'missing assignee';
    const index = this._assignees.indexOf(assignee);
    if (index >= 0) this._assignees.splice(index, 1);
  }

  updateAttribute(attribute) {
    this['_' + attribute.key] = attribute.value;
  }

}