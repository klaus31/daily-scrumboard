import ModelWithInstanceId from '../ModelWithInstanceId'
import moment from 'moment'
import StoryCreatedBuilder from "./builder/StoryCreatedBuilder";
import TeammemberAssignedToTaskBuilder from "./builder/TeammemberAssignedToTaskBuilder";
import TeammemberRemovedFromTaskBuilder from "./builder/TeammemberRemovedFromTaskBuilder";
import StoryChangedBuilder from "./builder/StoryChangedBuilder";
import TaskCreatedBuilder from "./builder/TaskCreatedBuilder";
import TaskChangedBuilder from "./builder/TaskChangedBuilder";
import TaskToStoryBuilder from "./builder/TaskToStoryBuilder";
import Attribute from "../board/Attribute";
import StateOfTask from "../board/StateOfTask";

export default class BoardEvent extends ModelWithInstanceId {

  constructor(type, instanceId, date, subject) {
    super(instanceId);
    this._type = type;
    this._date = date;
    this._subject = subject;
  }

  get type() {
    return this._type;
  }

  get date() {
    return this._date;
  }

  get subject() {
    return this._subject;
  }

  get dateAndTimePretty() {
    return moment(this._date).format('DD.MM.YYYY HH:mm') + ' Uhr';
  }

  occuredBefore(other) {
    let dateToCheck = other;
    if (other instanceof BoardEvent) dateToCheck = other.date;
    return moment(this._date).isBefore(dateToCheck);
  }

  occuredAfter(other) {
    let dateToCheck = other;
    if (other instanceof BoardEvent) dateToCheck = other.date;
    return moment(this._date).isAfter(dateToCheck);
  }

  occuredAt(date) {
    return moment(this._date).isSame(moment(date));
  }

  occuredAtSameDay(day) {
    return this.day.isSame(moment(day));
  }

  get day() {
    return moment(moment(this._date).format('YYYY-MM-DD'))
  }

  concernsStory(story) {
    if (story.instanceId) return this._subject.concernsStory(story.instanceId);
    else return this._subject.concernsStory(story);
  }

  concernsTask(task) {
    const instanceIdTask = task.instanceId || task;
    return this._subject.concernsTask(instanceIdTask);
  }

  get instanceIdTask() {
    return this._subject.instanceIdTask;
  }

  get instanceIdStory() {
    return this._subject.instanceIdStory;
  }

  get instanceIdCard() {
    return this._subject.instanceIdCard;
  }

  concernsSameCardAs(other) {
    return this.instanceIdCard === other.instanceIdCard;
  }

  toJSON() {
    const json = super.toJSON();
    json.type = this._type.key;
    json.date = this._date.utcOffset(0, true).format();
    json.subject = this._subject.toJSON();
    return json;
  }

  get isATaskMove() {
    return this.type.key === 'TASK_CREATED' || this.type.key === 'TASK_TO_STORY' || this.type.key === 'TASK_CHANGED' && this.subject.properties && this.subject.properties.attribute.key === 'state'
  }

  get prettySummary() {

    const calcKeyValuePretty = function (prettyKey, value) {
      if (!value) {
        return `${prettyKey} gelöscht`;
      } else {
        return `${prettyKey} geändert zu "${value}"`;
      }
    };

    const calcTaskChangedInfo = function (taskChangedEvent) {
      let value = taskChangedEvent.subject.properties.attribute.value;
      if (value.constructor === StateOfTask) {
        return `Task verschoben zu "${value.name}"`;
      } else {
        return calcKeyValuePretty(taskChangedEvent.subject.properties.attribute.prettyKey, value);
      }
    };

    switch (this.type.key) {
      case 'STORY_CREATED':
        return `Story "${this.subject.properties.label}" erstellt`;
      case 'TASK_CREATED':
        return `Task "${this.subject.properties.label}" erstellt`;
      case 'STORY_CHANGED':
        return calcKeyValuePretty(this.subject.properties.attribute.prettyKey, this.subject.properties.attribute.value);
      case 'TASK_CHANGED':
        return calcTaskChangedInfo(this);
      case 'TASK_TO_STORY':
        return `Task einer anderen Story zugeordnet`;
      case 'TASK_TEAMMEMBER_ASSIGNED':
        return `"${this.subject.properties.teammember.name}" zugewiesen`;
      case 'TASK_TEAMMEMBER_REMOVED':
        return `"${this.subject.properties.teammember.name}" entfernt`;
      default:
        throw 'Unbekanntes Event: ' + this.type.key;
    }

  }

  static fromJSON(json, teammembers) {

    let builder;

    switch (json.type) {
      case 'STORY_CREATED':
        builder = new StoryCreatedBuilder()
          .label(json.subject.label);
        break;
      case 'STORY_CHANGED':
        builder = new StoryChangedBuilder()
          .instanceIdStory(json.subject.instanceIdStory)
          .attribute(Attribute.fromJSON(json.subject));
        break;
      case 'TASK_CREATED':
        builder = new TaskCreatedBuilder()
          .instanceIdStory(json.subject.instanceIdStory)
          .label(json.subject.label);
        break;
      case 'TASK_TO_STORY':
        builder = new TaskToStoryBuilder()
          .instanceIdStory(json.subject.instanceIdStory)
          .instanceIdTask(json.subject.instanceIdTask);
        break;
      case 'TASK_CHANGED':
        builder = new TaskChangedBuilder()
          .instanceIdStory(json.subject.instanceIdStory)
          .instanceIdTask(json.subject.instanceIdTask)
          .attribute(Attribute.fromJSON(json.subject));
        break;
      case 'TASK_TEAMMEMBER_ASSIGNED':
        builder = new TeammemberAssignedToTaskBuilder()
          .instanceIdStory(json.subject.instanceIdStory)
          .instanceIdTask(json.subject.instanceIdTask)
          .teammember(teammembers.filter(teammember => teammember.key === json.subject.teammember)[0]);
        break;
      case 'TASK_TEAMMEMBER_REMOVED':
        builder = new TeammemberRemovedFromTaskBuilder()
          .instanceIdStory(json.subject.instanceIdStory)
          .instanceIdTask(json.subject.instanceIdTask)
          .teammember(teammembers.filter(teammember => teammember.key === json.subject.teammember)[0]);
        break;
      default:
        throw 'Unbekanntes Event: ' + json.type;
    }

    return builder.instanceId(json.instanceId)
      .date(moment(json.date))
      .build();
  }

}