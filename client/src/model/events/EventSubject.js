export default class EventSubject {

  constructor(instanceIdStory, properties, propertiesToJson) {
    this._instanceIdStory = instanceIdStory;
    this._properties = properties;
    this._propertiesToJson = propertiesToJson || function(p){return p;}
  }

  get properties() {
    return this._properties;
  }

  concernsStory(instanceIdStory) {
    return instanceIdStory === this._instanceIdStory;
  }

  concernsTask(instanceIdTask) {
    return instanceIdTask === this.instanceIdTask;
  }

  get instanceIdStory() {
    return this._instanceIdStory;
  }

  get instanceIdTask() {
    return this._properties ? this._properties.instanceIdTask : null;
  }

  get instanceIdCard() {
    return this.instanceIdTask || this.instanceIdStory;
  }

  toJSON() {
    const json = this._propertiesToJson(this._properties);
    json.instanceIdStory = this._instanceIdStory;
    return json;
  }

  static fromJSON(json) {
    throw `converting ${json} not implemented yet`;
  }

}