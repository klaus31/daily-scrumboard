export default class EventType {

  constructor(key, label) {
    this._key = key;
    this._label = label;
  }

  get key() {
    return this._key;
  }

  get label() {
    return this._label;
  }

  toJSON() {
    return {
      key: this._key,
      label: this._label,
    };
  }

  static fromJSON(json) {
    throw `converting ${json} not implemented yet`;
  }

}