import EventType from './../EventType'
import EventSubject from './../EventSubject'
import BoardEvent from './../BoardEvent'

export default class StoryChangedBuilder {

  constructor() {
    this._instanceId = '';
    this._instanceIdStory = '';
    this._date = '';
    this._attribute = null;
  }

  attribute(attribute) {
    this._attribute = attribute;
    return this;
  }

  date(date) {
    this._date = date;
    return this;
  }

  instanceId(instanceId) {
    this._instanceId = instanceId;
    return this;
  }

  instanceIdStory(instanceIdStory) {
    this._instanceIdStory = instanceIdStory;
    return this;
  }

  static _propertiesToJson() {
    return function(properties) {
      return properties.attribute.toJSON();
    }
  }

  build() {
    if (!this._instanceIdStory) throw 'missing instanceIdStory';
    if (!this._date) throw 'missing date';
    if (!this._attribute) throw 'missing attribute';
    const eventType = new EventType('STORY_CHANGED', 'Story geändert');
    const subject = new EventSubject(this._instanceIdStory, {attribute: this._attribute}, StoryChangedBuilder._propertiesToJson());
    return new BoardEvent(eventType, this._instanceId, this._date, subject);
  }
}