import EventType from './../EventType'
import EventSubject from './../EventSubject'
import BoardEvent from './../BoardEvent'

export default class StoryCreatedBuilder {

  constructor() {
    this._instanceId = '';
    this._date = '';
    this._label = '';
  }

  label(label) {
    this._label = label;
    return this;
  }

  date(date) {
    this._date = date;
    return this;
  }

  instanceId(instanceId) {
    this._instanceId = instanceId;
    return this;
  }

  build() {
    if (!this._date) throw 'missing date';
    if (!this._label) throw 'missing label';
    const eventType = new EventType('STORY_CREATED', 'Story erstellt');
    const subject = new EventSubject(this._instanceId, {label: this._label, instanceIdStory: this._instanceId});
    return new BoardEvent(eventType, this._instanceId, this._date, subject);
  }
}