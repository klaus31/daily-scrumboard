import EventType from './../EventType'
import EventSubject from './../EventSubject'
import BoardEvent from './../BoardEvent'

export default class TaskChangedBuilder {

  constructor() {
    this._instanceId = '';
    this._instanceIdStory = '';
    this._instanceIdTask = '';
    this._date = '';
    this._attribute = null;
  }

  attribute(attribute) {
    this._attribute = attribute;
    return this;
  }

  date(date) {
    this._date = date;
    return this;
  }

  instanceId(instanceId) {
    this._instanceId = instanceId;
    return this;
  }

  instanceIdStory(instanceIdStory) {
    this._instanceIdStory = instanceIdStory;
    return this;
  }

  instanceIdTask(instanceIdTask) {
    this._instanceIdTask = instanceIdTask;
    return this;
  }

  static _propertiesToJson() {
    return function(properties) {
      const json = properties.attribute.toJSON();
      json.instanceIdTask = properties.instanceIdTask;
      return json;
    }
  }

  build() {
    if (!this._instanceIdStory) throw 'missing instanceIdStory';
    if (!this._instanceIdTask) throw 'missing instanceIdTask';
    if (!this._date) throw 'missing date';
    if (!this._attribute) throw 'missing attribute';
    const eventType = new EventType('TASK_CHANGED', 'Task geändert');
    const subject = new EventSubject(this._instanceIdStory, {attribute: this._attribute, instanceIdTask: this._instanceIdTask}, TaskChangedBuilder._propertiesToJson());
    return new BoardEvent(eventType, this._instanceId, this._date, subject);
  }
}