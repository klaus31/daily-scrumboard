import EventType from './../EventType'
import EventSubject from './../EventSubject'
import BoardEvent from './../BoardEvent'

export default class TaskCreatedBuilder {

  constructor() {
    this._instanceId = '';
    this._instanceIdStory = '';
    this._date = '';
    this._label = '';
  }

  label(label) {
    this._label = label;
    return this;
  }

  date(date) {
    this._date = date;
    return this;
  }

  instanceId(instanceId) {
    this._instanceId = instanceId;
    return this;
  }

  instanceIdStory(instanceIdStory) {
    this._instanceIdStory = instanceIdStory;
    return this;
  }

  build() {
    if (!this._instanceIdStory) throw 'missing instanceIdStory';
    if (!this._date) throw 'missing date';
    if (!this._label) throw 'missing label';
    const eventType = new EventType('TASK_CREATED', 'Task erstellt');
    const subject = new EventSubject(this._instanceIdStory, {label: this._label, instanceIdTask: this._instanceId, instanceIdStory: this._instanceIdStory});
    return new BoardEvent(eventType, this._instanceId, this._date, subject);
  }
}