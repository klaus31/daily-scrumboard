import EventType from './../EventType'
import EventSubject from './../EventSubject'
import BoardEvent from './../BoardEvent'

export default class TaskToStoryBuilder {

  constructor() {
    this._instanceId = '';
    this._instanceIdStory = '';
    this._instanceIdTask = '';
    this._date = '';
  }

  date(date) {
    this._date = date;
    return this;
  }

  instanceId(instanceId) {
    this._instanceId = instanceId;
    return this;
  }

  instanceIdStory(instanceIdStory) {
    this._instanceIdStory = instanceIdStory;
    return this;
  }

  instanceIdTask(instanceIdTask) {
    this._instanceIdTask = instanceIdTask;
    return this;
  }

  static _propertiesToJson() {
    return function (properties) {
      return {
        instanceIdTask: properties.instanceIdTask
      };
    }
  }

  build() {
    if (!this._instanceIdStory) throw 'missing instanceIdStory';
    if (!this._instanceIdTask) throw 'missing instanceIdTask';
    if (!this._date) throw 'missing date';
    const eventType = new EventType('TASK_TO_STORY', 'Task einer Story zugeordnet');
    const subject = new EventSubject(this._instanceIdStory, {instanceIdTask: this._instanceIdTask}, TaskToStoryBuilder._propertiesToJson());
    return new BoardEvent(eventType, this._instanceId, this._date, subject);
  }
}