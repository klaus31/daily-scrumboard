import EventType from './../EventType'
import EventSubject from './../EventSubject'
import BoardEvent from './../BoardEvent'

export default class TeammemberRemovedFromTaskBuilder {

  constructor() {
    this._instanceId = '';
    this._instanceIdTask = '';
    this._instanceIdStory = '';
    this._date = '';
    this._teammember = '';
  }

  teammember(teammember) {
    this._teammember = teammember;
    return this;
  }

  date(date) {
    this._date = date;
    return this;
  }

  instanceId(instanceId) {
    this._instanceId = instanceId;
    return this;
  }

  instanceIdTask(instanceIdTask) {
    this._instanceIdTask = instanceIdTask;
    return this;
  }

  instanceIdStory(instanceIdStory) {
    this._instanceIdStory = instanceIdStory;
    return this;
  }

  static _propertiesToJson() {
    return function (properties) {
      return {
        teammember: properties.teammember.key,
        instanceIdTask: properties.instanceIdTask
      };
    }
  }

  build() {
    if (!this._instanceIdTask) throw 'missing instanceIdTask';
    if (!this._instanceIdStory) throw 'missing instanceIdStory';
    if (!this._date) throw 'missing date';
    if (!this._teammember) throw 'missing teammember';
    const eventType = new EventType('TASK_TEAMMEMBER_REMOVED', 'Benutzer entfernt');
    const subject = new EventSubject(this._instanceIdStory, {teammember: this._teammember, instanceIdTask: this._instanceIdTask}, TeammemberRemovedFromTaskBuilder._propertiesToJson());
    return new BoardEvent(eventType, this._instanceId, this._date, subject);
  }
}