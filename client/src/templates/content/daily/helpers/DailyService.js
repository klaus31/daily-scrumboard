import moment from 'moment';
import CONFIG from "./../../../../config";

export default class DailyService {

  constructor() {
  }

  searchEndOfLastDaily() {
    let result = moment().subtract(1, 'day');
    while (this._isOffWorkTime(result)) {
      result.subtract(1, 'day');
    }
    result.set({
      hour: CONFIG.DAILY.END.HOUR,
      minute: CONFIG.DAILY.END.MINUTE,
      second: 0,
      millisecond: 0
    });
    return result;
  }

  _isOffWorkTime(moment) {
    return moment.format('dddd') === 'Samstag' || moment.format('dddd') === 'Sonntag' || CONFIG.TIME_OFF_WORK.some(timeOffDay => moment.isSame(timeOffDay, 'day'))
  }

  get isDailyRunning() {
    const now = moment();
    if(this._isOffWorkTime(now)) return false;
    const dailyStartHour = CONFIG.DAILY.START.HOUR;
    const dailyStartMinute = CONFIG.DAILY.START.MINUTE;
    const dailyEndHour = CONFIG.DAILY.END.HOUR;
    const dailyEndMinute = CONFIG.DAILY.END.MINUTE;
    const nowHour = now.toDate().getHours();
    const nowMinute = now.toDate().getMinutes();
    if(nowHour < dailyStartHour) return false;
    if(nowHour > dailyEndHour) return false;
    if(nowMinute < dailyStartMinute) return false;
    if(nowMinute > dailyEndMinute) return false;
    return true;
  }

  get dailyEndTimePretty() {
    const minutes = CONFIG.DAILY.END.MINUTE < 10 ? `0${CONFIG.DAILY.END.MINUTE}` : CONFIG.DAILY.END.MINUTE;
    return `${CONFIG.DAILY.END.HOUR}:${minutes} Uhr`;
  }
}