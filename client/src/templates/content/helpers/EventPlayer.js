import Story from '../../../model/board/Story'
import Task from '../../../model/board/Task'
import SETTINGS from '../../../model/Settings'
import ACTIVE_CARD_CHANGED_LISTENER from '../historie/helpers/ActiveCardChangedListener';

/**
 * Die Idee dieser Klasse ist, dass hier das Steuern des Boards wie bei einem Musikplayer übernommen werden kann.
 * So kann die Ansicht vorgespult, abgespielt, pausiert, zurückgespult werden etc.
 *
 * XXX Schöne Idee und Musik ist ja immer toll. Am Ende ist das hier aber auch nur ein stinknormaler Controller ...
 */
export default class EventPlayer {

  constructor(board, events) {
    this._events = events || [];
    this._board = board;
    this._latestSortStrategy = 'time-global';
  }

  loadEvents(events) {
    this._events = events;
  }

  updateEvents(events) {
    events.forEach(e => this.pushEvent(e));
  }

  pushEvent(event) {
    const exists = this._events.some(e => e.instanceId === event.instanceId);
    if (!exists) this._events.push(event);
  }

  set sortByStrategy(strategy) {
    this._latestSortStrategy = strategy;
    if (strategy === 'per-swim-lane') this._sortByStorySwimlane();
    else if (strategy === 'per-swim-lane-and-tasks') this._sortByStorySwimlaneAndTasks();
    else this._sortByTimeGlobal();
  }

  _sortByStorySwimlaneAndTasks() {
    const taskInstanceIds = this._events.filter(e => e.type.key === 'TASK_CREATED').map(e => e.instanceIdTask);
    this._sortByStorySwimlaneAnd((e1, e2) => {
      if (e1 === e2) return 0;
      const indexOfE1 = taskInstanceIds.indexOf(e1.instanceIdTask);
      const indexOfE2 = taskInstanceIds.indexOf(e2.instanceIdTask);
      if (indexOfE1 >= 0) {
        if (indexOfE2 >= 0) {
          if (indexOfE1 === indexOfE2) {
            return e1.occuredBefore(e2) ? -1 : 1;
          } else {
            return indexOfE1 < indexOfE2 ? -1 : 1;
          }
        } else {
          return 1;
        }
      }
      else if (indexOfE2 >= 0) {
        return -1;
      } else {
        return 0;
      }
    });
  }

  _sortByStorySwimlaneAnd(sortTasksFunc) {
    const result = [];
    // Suche nach STORY_CREATED und sortiere diese (Neuste zuerst)
    this._events
      .filter(e => e.type.key === 'STORY_CREATED')
      .sort((e1, e2) => e1.occuredBefore(e2) ? -1 : 1)
      .forEach(storyCreated => {
        // Hänge die betreffenden Events jeweils dahinter
        result.push(storyCreated);
        this._events
          .filter(event => event.concernsStory(storyCreated))
          .filter(event => event !== storyCreated)
          .sort(sortTasksFunc)
          .forEach(e => result.push(e));
      });
    this._events = result;
  }

  _sortByTimeGlobal() {
    this._events = this._events.sort((e1, e2) => e1.occuredBefore(e2) ? -1 : 1);
  }

  _sortByStorySwimlane() {
    this._sortByStorySwimlaneAnd((e1, e2) => e1.occuredBefore(e2) ? -1 : 1);
  }

  hasPrevEvent(event) {
    return this._events.indexOf(event) > 0;
  }

  hasNextEvent(event) {
    const nextEventIndex = this._events.indexOf(event) + 1;
    return this._events.length > nextEventIndex;
  }

  static _handleStoryCreated(stories, event, targetEvent, skipActivation) {
    const story1 = new Story(event.instanceId, event.subject.properties.label);
    if (!skipActivation) story1.active = event === targetEvent;
    stories.push(story1);
    return story1;
  }

  goToLastEvent(skipActivation) {
    return this.goToEvent(this.getLastEvent(), skipActivation);
  }

  getLastEvent() {
    return this._events[this._events.length - 1];
  }

  searchEventWithInstanceId(instanceId) {
    return this._events.filter(e => e.instanceId === instanceId)[0];
  }

  hasEventWithInstanceId(instanceId) {
    return this._events.some(e => e.instanceId === instanceId);
  }

  findEventWithInstanceId(instanceId) {
    return this._events.filter(e => e.instanceId === instanceId).pop()
  }

  findTaskAtEvent(event, instanceId) {
    const tasks = this.goToEvent(event, true)
      .map(story => story.tasks)
      .flat()
      .filter(task => task.instanceId === instanceId);
    return tasks.length ? tasks[0] : null;
  }

  goToEvent(targetEvent, skipActivation) {
    const stories = [];
    let activeCard = null;
    this._events
      .slice(0, this._events.indexOf(targetEvent) + 1)
      .forEach(event => {
        switch (event.type.key) {
          case 'STORY_CREATED':
            const card = EventPlayer._handleStoryCreated(stories, event, targetEvent, skipActivation); // eslint-disable-line no-case-declarations
            if (!skipActivation) activeCard = card;
            break;
          case 'STORY_CHANGED':
            stories
              .filter(story => event.concernsStory(story))
              .forEach(story => {
                story.updateAttribute(event.subject.properties.attribute);
                if (!skipActivation) story.active = event === targetEvent;
                if (!skipActivation) activeCard = story;
              });
            break;
          case 'TASK_CREATED':
            const firstStateOfTask = this._board.firstStateOfTask; // eslint-disable-line no-case-declarations
            stories.filter(story => event.concernsStory(story))
              .forEach(story => {
                const task = new Task(event.instanceId, event.subject.properties.label, firstStateOfTask, story); // eslint-disable-line no-case-declarations
                story.addTask(task);
                if (!skipActivation) task.active = event === targetEvent;
                if (!skipActivation) activeCard = task;
              });
            break;
          case 'TASK_CHANGED':
            stories.filter(story => event.concernsStory(story))
              .forEach(story => {
                story.tasks
                  .filter(task => task.instanceId === event.subject.properties.instanceIdTask)
                  .forEach(task => {
                    if (!skipActivation) task.active = event === targetEvent;
                    task.updateAttribute(event.subject.properties.attribute);
                    if (!skipActivation) activeCard = task;
                  });
              });
            break;
          case 'TASK_TO_STORY':
            stories.filter(story => event.concernsStory(story)).forEach(newStory => {
              const task = stories.map(story => story.tasks).flat().filter(task => event.concernsTask(task))[0];
              if (!skipActivation) task.active = event === targetEvent;
              task.story = newStory;
              if (!skipActivation) activeCard = task;
            });
            break;
          case 'TASK_TEAMMEMBER_ASSIGNED':
            stories.map(story => story.tasks)
              .flat()
              .filter(task => task.instanceId === event.subject.properties.instanceIdTask)
              .forEach(task => {
                task.addAssignee(event.subject.properties.teammember);
                if (!skipActivation) task.active = event === targetEvent;
                if (!skipActivation) activeCard = task;
              });
            break;
          case 'TASK_TEAMMEMBER_REMOVED':
            stories.map(story => story.tasks)
              .flat()
              .filter(task => task.instanceId === event.subject.properties.instanceIdTask)
              .forEach(task => {
                task.removeAssignee(event.subject.properties.teammember);
                if (!skipActivation) task.active = event === targetEvent;
                if (!skipActivation) activeCard = task;
              });
            break;
          default:
            throw 'Unbekanntes Event: ' + event.type.key;
        }
      });
    if (activeCard) ACTIVE_CARD_CHANGED_LISTENER.fire(activeCard);
    let result = stories.sort((s1, s2) => s1.order > s2.order ? -1 : 1);
    if (!SETTINGS.showDeletedCards) {
      result = result.filter(story => SETTINGS.showDeletedCards || !story.deleted);
      result.forEach(s => s.removeDeletedTasks());
    }
    result.forEach(story => story.tasks.sort((t1, t2) => t1.hasHigherPrioThan(t2) ? -1 : 1));
    return result;
  }

  searchEventToShow() {
    if (localStorage.getItem('lastEventDate')) {
      const dateString = localStorage.getItem('lastEventDate');
      const events = this._events.filter(event => event.occuredAt(dateString));
      // Es könnte sein, dass das gespeicherte Event nicht mehr existiert.
      // Dann Fallback und zurück auf 0.
      return events.length ? events[0] : this._events[0];
    } else {
      return this._events[0];
    }
  }

  searchEventsConcerningTask(task) {
    return this._events.filter(e => e.concernsTask(task));
  }

  searchEventsConcerningTasks(tasks) {
    return this._events.filter(e => tasks.some(task => e.concernsTask(task)));
  }

  searchEventsConcerningStory(story) {
    return this._events.filter(e => e.concernsStory(story));
  }

  loadFirstEvent() {
    return this._events[0];
  }

  loadPrevEvent(event) {
    if (!this.hasPrevEvent(event)) return;
    return this._events[this._events.indexOf(event) - 1];
  }

  loadNextEvent(event) {
    if (!this.hasNextEvent(event)) return;
    return this._events[this._events.indexOf(event) + 1];
  }

  loadLastEvent() {
    return this.getLastEvent();
  }

  loadFirstEventOn(day) {
    this._sortByTimeGlobal();
    // day has hour 00:00:00. Thus we can use "occuredAfter".
    const eventsOnDayAndAfter = this._events.filter(e => e.occuredAfter(day));
    if (eventsOnDayAndAfter.length) {
      this.sortByStrategy = this._latestSortStrategy;
      return eventsOnDayAndAfter[0].occuredAtSameDay(day) ? eventsOnDayAndAfter[0] : null;
    }
  }

  loadEventCurrentStateOfCard(currentEvent) {
    if (!currentEvent) throw 'currentEvent needed';
    let nextEvent = this.loadNextEvent(currentEvent);
    if (!nextEvent) return;
    if (nextEvent.concernsSameCardAs(currentEvent)) {
      while (nextEvent && nextEvent.concernsSameCardAs(currentEvent)) nextEvent = this.loadNextEvent(nextEvent);
      return nextEvent ? this.loadPrevEvent(nextEvent) : this.loadLastEvent();
    } else {
      return nextEvent;
    }
  }

  loadEventFirstStateOfCard(currentEvent) {
    if (!currentEvent) throw 'currentEvent needed';
    let prevEvent = this.loadPrevEvent(currentEvent);
    if (!prevEvent) return;
    if (prevEvent.concernsSameCardAs(currentEvent)) {
      while (prevEvent && prevEvent.concernsSameCardAs(currentEvent)) prevEvent = this.loadPrevEvent(prevEvent);
      return prevEvent ? this.loadNextEvent(prevEvent) : this.loadFirstEvent();
    } else {
      return prevEvent;
    }
  }
}