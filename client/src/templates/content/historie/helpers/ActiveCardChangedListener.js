class ActiveCardChangedListener {

  constructor() {
    this._consumers = [];
  }

  register(consumer) {
    this._consumers.push(consumer);
  }

  fire(storyOrTask) {
    this._consumers.forEach(func => func(storyOrTask));
  }

}

// Singleton!
export default new ActiveCardChangedListener()