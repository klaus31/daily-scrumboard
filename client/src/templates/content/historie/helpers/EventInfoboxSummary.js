import Story from '../../../../model/board/Story'
import Task from '../../../../model/board/Task'

export default class EventInfoboxSummary {

  constructor() {
    this._summary = {
      label: '',
      assignees: [],
      state: '',
      branch: '',
      text: ''
    };
  }

  get summary() {
    return this._summary;
  }

  onChange(taskOrStory) {
    if (taskOrStory.constructor === Story) {
      this.recalculateSummaryOfStory(taskOrStory);
    } else if (taskOrStory.constructor === Task) {
      this.recalculateSummaryOfTask(taskOrStory);
    }
  }

  recalculateSummaryOfStory(story) {
    this._summary.label = story.label.length > 33 ? story.label.substr(0, 30) + ' ...' : story.label;
    this._summary.text = story.description;
    this._summary.assignees = [];
    this._summary.state = '';
    this._summary.lockText = '';
    this._summary.ticketNumber = '';
    this._summary.branch = story.branch;
    this._summary.isStory = true;
    this._summary.effort = '';
    this._summary.prio = '';
    this._summary.isTask = false;
  }

  recalculateSummaryOfTask(task) {
    this._summary.label = task.label.length > 33 ? task.label.substr(0, 30) + ' ...' : task.label;
    this._summary.text = task.description;
    this._summary.assignees = task.assignees;
    this._summary.state = task.state.name;
    this._summary.branch = task.branch;
    this._summary.lockText = task.lockText;
    this._summary.ticketNumber = task.ticketNumber;
    this._summary.effort = task.pretty.effort;
    this._summary.prio = task.pretty.prioLong;
    this._summary.isStory = false;
    this._summary.isTask = true;
  }
}