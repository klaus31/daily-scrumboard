import CONFIG from './../../config'

class AuthentificationService {

  logout() {
    localStorage.setItem('login-pig', '');
    localStorage.setItem('login-chicken', '');
    localStorage.setItem('view', 'home');
    window.location.reload();
  }

  isLoggedIn() {
    return this.isLoggedInAsPig() || this.isLoggedInAsChicken();
  }

  isLoggedInAsChicken() {
    return CONFIG.LOGIN.CHICKEN === localStorage.getItem('login-chicken');
  }

  isLoggedInAsPig() {
    return CONFIG.LOGIN.PIG === localStorage.getItem('login-pig');
  }

  hasLogin() {
    return CONFIG.LOGIN.PIG || CONFIG.LOGIN.CHICKEN;
  }

  login(password) {
    if (password === CONFIG.LOGIN.CHICKEN) {
      localStorage.setItem('login-chicken', password);
      window.location.reload();
    }
    if (password === CONFIG.LOGIN.PIG) {
      localStorage.setItem('login-pig', password);
      window.location.reload();
    }
  }
}

export default new AuthentificationService();