import AUTHENTIFICATION_SERVICE from './AuthentificationService'

class AuthorizationService {

  get _isNoLoginOrAPig() {
    return !AUTHENTIFICATION_SERVICE.hasLogin() || AUTHENTIFICATION_SERVICE.isLoggedInAsPig();
  }

  get isAuthToWorkOnCode() {
    return this._isNoLoginOrAPig;
  }

  get isAuthToWorkOnBoard() {
    return this._isNoLoginOrAPig;
  }

  get isAuthAsChickenOnly() {
    return AUTHENTIFICATION_SERVICE.hasLogin() && AUTHENTIFICATION_SERVICE.isLoggedInAsChicken();
  }

  get isAuthToSendDailyClick() {
    return this._isNoLoginOrAPig;
  }

  isAuthToSee(view) {
    switch (view) {
      case 'home':
      case 'daily':
      case 'historie':
      case 'sprint':
      case 'settings':
      case 'comingup':
        return !AUTHENTIFICATION_SERVICE.hasLogin() || AUTHENTIFICATION_SERVICE.isLoggedIn();
      case 'working':
      case 'members':
      case 'eventoverview':
      case 'impediments':
      case 'teamactions':
      case 'policy':
        return this._isNoLoginOrAPig;
      default:
        throw `unknown view: ${view}`;
    }
  }
}

export default new AuthorizationService();