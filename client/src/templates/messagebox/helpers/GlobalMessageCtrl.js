class GlobalMessageCtrl {

  showMessage(message) {
    if (this._onNewMessage) {
      this._onNewMessage(message);
    }
  }

  onNewMessage(onNewMessage) {
    this._onNewMessage = onNewMessage;
  }
}

export default new GlobalMessageCtrl();