import TimeToReadCalculator from './TimeToReadCalculator'

const TTRC = new TimeToReadCalculator();

export default class Message {

  constructor(text, secondsToShow, callbackOnClose, type) {
    this._text = text;
    this._secondsToShow = secondsToShow;
    this._callbackOnClose = callbackOnClose;
    if(type && ['info', 'error'].indexOf(type) < 0) throw 'type muss info oder error sein';
    this._type = type || 'info';
  }

  static createInfoShownTimeBased(text) {
    const secondsToShow = TTRC.getSecondsToRead(text);
    return new Message(text, secondsToShow, null, 'info');
  }

  static createErrorShownTimeBased(text) {
    const secondsToShow = TTRC.getSecondsToRead(text);
    return new Message(text, secondsToShow, null, 'error');
  }

  static createErrorShownUntilClosed(text, callbackOnClose) {
    return new Message(text, null, callbackOnClose, 'error');
  }

  get isInfo() {
    return this._type === 'info';
  }

  get isError() {
    return this._type === 'error';
  }

  get text() {
    return this._text;
  }

  get secondsToShow() {
    return this._secondsToShow;
  }

  get callbackOnClose() {
    return this._callbackOnClose || function () {
    };
  }

  get isCloseable() {
    return this._callbackOnClose !== null;
  }

  get isAutoHide() {
    return !!this._secondsToShow;
  }

}