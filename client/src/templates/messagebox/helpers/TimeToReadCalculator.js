/**
 * Calculate the time to read a text.
 */
export default class TimeToReadCalculator {

  /**
   * @param delay seconds the user gets extra time for every text. Default ist 2.
   * @param wordsPerMinute A human can read 200 to 300 words the minute in average.
   * Use 100 here as default (to match slow and sleeping users)
   */
  constructor(delay, wordsPerMinute) {
    this._delay = delay || 2;
    this._wordsPerMinute = wordsPerMinute || 100;
  }

  getSecondsToRead(text) {
    const words = text.match(/\s+/g).length + 1;
    return Math.round(words * 60 / this._wordsPerMinute) + this._delay;
  }
}