package com.gitlab.klaus31.scrumboard.boards;

import java.util.List;

class Board {

    private final List<StateOfTask> possibleStates;
    private final List<Teammember> teammembers;

    Board(List<StateOfTask> possibleStates, List<Teammember> teammembers) {
        this.possibleStates = possibleStates;
        this.teammembers = teammembers;
    }

    public List<StateOfTask> getPossibleStates() {
        return possibleStates;
    }

    public List<Teammember> getTeammembers() {
        return teammembers;
    }

    public static final class Builder {
        private List<StateOfTask> possibleStates;
        private List<Teammember> teammembers;

        private Builder() {
        }

        public static Builder aBoard() {
            return new Builder();
        }

        public Builder withPossibleStates(List<StateOfTask> possibleStates) {
            this.possibleStates = possibleStates;
            return this;
        }

        public Builder withTeammembers(List<Teammember> teammembers) {
            this.teammembers = teammembers;
            return this;
        }

        public Board build() {
            return new Board(possibleStates, teammembers);
        }
    }
}
