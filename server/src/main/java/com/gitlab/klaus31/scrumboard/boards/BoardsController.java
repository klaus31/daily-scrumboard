package com.gitlab.klaus31.scrumboard.boards;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import static com.gitlab.klaus31.scrumboard.boards.Board.Builder.aBoard;

@RestController
public class BoardsController {

    private final TeammemberRepository teammemberRepository;
    private final StateOfTaskRepository stateOfTaskRepository;

    public BoardsController(TeammemberRepository teammemberRepository, StateOfTaskRepository stateOfTaskRepository) {
        this.teammemberRepository = teammemberRepository;
        this.stateOfTaskRepository = stateOfTaskRepository;
    }

    @GetMapping("/boards")
    @CrossOrigin
    public Collection<Board> boards() {
        List<Teammember> teammembers = fetchTeammembers();
        List<StateOfTask> possibleStates = fetchPossibleStates();
        Board board = aBoard()
                .withPossibleStates(possibleStates)
                .withTeammembers(teammembers)
                .build();
        List<Board> boards = Collections.singletonList(board);
        HttpStatus status = HttpStatus.OK;
        return boards;
    }

    private List<StateOfTask> fetchPossibleStates() {
        return stateOfTaskRepository.findAllByOrderByKeyAsc();
    }

    private List<Teammember> fetchTeammembers() {
        List<Teammember> target = new ArrayList<>();
        teammemberRepository.findAll().forEach(target::add);
        return target;
    }
}
