package com.gitlab.klaus31.scrumboard.boards;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class StateOfTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;
    public int key;
    public String name;

}
