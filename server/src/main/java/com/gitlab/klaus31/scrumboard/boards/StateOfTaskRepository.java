package com.gitlab.klaus31.scrumboard.boards;

import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface StateOfTaskRepository extends CrudRepository<StateOfTask, Long> {

    List<StateOfTask> findAllByOrderByKeyAsc();
}