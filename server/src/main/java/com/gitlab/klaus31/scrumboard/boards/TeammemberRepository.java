package com.gitlab.klaus31.scrumboard.boards;

import org.springframework.data.repository.CrudRepository;

public interface TeammemberRepository extends CrudRepository<Teammember, Long> {

    Teammember findAllByKey(String key);
}