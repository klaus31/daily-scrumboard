package com.gitlab.klaus31.scrumboard.comingup;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.util.JSONPObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class ComingupController {

    private final ComingupRepository comingupRepository;

    public ComingupController(ComingupRepository comingupRepository) {
        this.comingupRepository = comingupRepository;
    }

    @GetMapping("/comingup")
    @CrossOrigin
    public Iterable<ComingupItem> comingupItems() {
        return comingupRepository.findAll();
    }

    @PostMapping("/comingup")
    @CrossOrigin
    public ResponseEntity<Object> replaceComingUpItems(@RequestBody ArrayNode itemsPosted) {
        comingupRepository.deleteAll();
        List<ComingupItem> items = new ArrayList<>();
        int i = 0;
        for (JsonNode itemPosted : itemsPosted) {
            ComingupItem item = new ComingupItem();
            item.name = itemPosted.get("name").asText();
            Optional.ofNullable(itemPosted.get("ticketNumber"))
                    .filter(node -> !node.isNull())
                    .map(JsonNode::asText)
                    .map(String::trim)
                    .filter(str -> !str.isEmpty())
                    .ifPresent(tn -> item.ticketNumber = tn);
            if (item.name == null || item.name.trim().equals("")) continue;
            item.sortNumber = i++;
            items.add(item);
        }
        comingupRepository.saveAll(items);
        return ResponseEntity.ok().build();
    }
}
