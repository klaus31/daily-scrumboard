package com.gitlab.klaus31.scrumboard.comingup;

import org.springframework.data.repository.CrudRepository;

public interface ComingupRepository extends CrudRepository<ComingupItem, Long> {
}