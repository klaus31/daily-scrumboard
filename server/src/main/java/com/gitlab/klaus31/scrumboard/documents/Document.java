package com.gitlab.klaus31.scrumboard.documents;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;

@TypeDef(typeClass = JsonStringType.class, defaultForType = JsonNode.class)
@Entity
public class Document {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;
    public String key;

    @Column(columnDefinition = "clob")
    public JsonNode json;
}
