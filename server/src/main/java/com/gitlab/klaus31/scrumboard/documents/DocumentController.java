package com.gitlab.klaus31.scrumboard.documents;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class DocumentController {

    private final DocumentRepository documentRepository;

    public DocumentController(DocumentRepository documentRepository) {
        this.documentRepository = documentRepository;
    }

    @GetMapping("/documents/{key}")
    @CrossOrigin
    public Optional<Document> document(@PathVariable(value = "key") String key) {
        return documentRepository.findByKey(key);
    }

    @PostMapping("/documents")
    @CrossOrigin
    public ResponseEntity<Document> replaceComingUpItems(@RequestBody Document document) {
        if (document.key == null || document.key.equals("")) return ResponseEntity.badRequest().build();
        Optional<Document> existingDocOpt = documentRepository.findByKey(document.key);
        if (existingDocOpt.isPresent()) {
            Document existingDoc = existingDocOpt.get();
            existingDoc.json = document.json;
            documentRepository.save(existingDoc);
        } else {
            documentRepository.save(document);
        }
        return ResponseEntity.ok().body(document);
    }
}
