package com.gitlab.klaus31.scrumboard.documents;

import com.gitlab.klaus31.scrumboard.comingup.ComingupItem;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface DocumentRepository extends CrudRepository<Document, Long> {

    Optional<Document> findByKey(String key);
}