package com.gitlab.klaus31.scrumboard.events;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonStringType;
import org.hibernate.annotations.TypeDef;

import javax.persistence.*;
import java.time.LocalDateTime;

@TypeDef(typeClass = JsonStringType.class, defaultForType = JsonNode.class)
@Entity
public class BoardEvent {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public int id;
    @Enumerated(EnumType.STRING)
    public BoardEventType type;
    public LocalDateTime date;
    public String instanceId;

    @Column(columnDefinition = "clob")
    public JsonNode subject;

}
