package com.gitlab.klaus31.scrumboard.events;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface BoardEventRepository extends CrudRepository<BoardEvent, Long> {
    List<BoardEvent> findAllByOrderByDate();

    Optional<BoardEvent> findFirstByOrderByDateDesc();
}