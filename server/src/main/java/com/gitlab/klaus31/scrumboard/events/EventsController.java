package com.gitlab.klaus31.scrumboard.events;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.gitlab.klaus31.scrumboard.sync.SyncService;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
public class EventsController {

    private final BoardEventRepository boardEventRepository;
    private final SyncService syncService;

    public EventsController(BoardEventRepository boardEventRepository, SyncService syncService) {
        this.boardEventRepository = boardEventRepository;
        this.syncService = syncService;
    }

    @DeleteMapping("/events")
    @CrossOrigin
    public ObjectNode deleteAllEvents() {
        boardEventRepository.deleteAll();
        syncService.deleteEverything();
        return new ObjectMapper().createObjectNode();
    }

    @PostMapping("/events")
    @CrossOrigin
    public ObjectNode postEvent(@RequestBody BoardEvent event) {
        event.instanceId = UUID.randomUUID().toString();
        boardEventRepository.save(event);

        ObjectNode response = new ObjectMapper().createObjectNode();
        response.set("currentInstanceId", new TextNode(event.instanceId));
        syncService.pustNewEvent(event);
        return response;
    }

    @GetMapping("/events")
    @CrossOrigin
    public List<BoardEvent> getEvents() {
        return boardEventRepository.findAllByOrderByDate();
    }
}
