package com.gitlab.klaus31.scrumboard.sync;

import com.gitlab.klaus31.scrumboard.events.BoardEvent;

import java.util.ArrayList;
import java.util.List;

public class SyncItem {

    private final List<BoardEvent> lastBoardEvents;
    private final Daily daily;

    public SyncItem() {
        this.lastBoardEvents = new ArrayList<>();
        this.daily = new Daily();
    }

    public void add(BoardEvent event) {
        this.lastBoardEvents.add(event);
    }

    public List<BoardEvent> getLastBoardEvents() {
        return lastBoardEvents;
    }

    public Daily getDaily() {
        return daily;
    }
}
