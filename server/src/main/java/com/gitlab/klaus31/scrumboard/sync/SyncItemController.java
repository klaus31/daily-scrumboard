package com.gitlab.klaus31.scrumboard.sync;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.web.bind.annotation.*;

@RestController
public class SyncItemController {

    private final SyncService syncService;

    public SyncItemController(SyncService syncService) {
        this.syncService = syncService;
    }

    @GetMapping("/syncitem")
    @CrossOrigin
    public SyncItem getSyncItem() {
        return syncService.getSyncItem();
    }

    @PostMapping("/syncitem")
    @CrossOrigin
    public ObjectNode postDailyInstanceId(@RequestBody Daily daily) {
        syncService.setInstaceId(daily.currentInstanceId);
        return new ObjectMapper().createObjectNode();
    }
}
