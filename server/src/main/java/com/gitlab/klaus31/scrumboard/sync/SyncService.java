package com.gitlab.klaus31.scrumboard.sync;

import com.gitlab.klaus31.scrumboard.events.BoardEvent;
import org.springframework.stereotype.Service;

@Service
public class SyncService {

    private SyncItem syncItem;

    public SyncService() {
        this.syncItem = new SyncItem();
    }

    public void deleteEverything() {
        this.syncItem = new SyncItem();
    }

    public void pustNewEvent(BoardEvent event) {
        this.syncItem.add(event);
    }

    public SyncItem getSyncItem() {
        return syncItem;
    }

    public void setInstaceId(String instanceId) {
        this.syncItem.getDaily().currentInstanceId = instanceId;
    }
}
