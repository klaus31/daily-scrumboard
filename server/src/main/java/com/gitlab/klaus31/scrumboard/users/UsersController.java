package com.gitlab.klaus31.scrumboard.users;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.gitlab.klaus31.scrumboard.boards.Teammember;
import com.gitlab.klaus31.scrumboard.boards.TeammemberRepository;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import javax.validation.Valid;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import java.util.UUID;

import static java.nio.charset.StandardCharsets.UTF_8;

@RestController
public class UsersController {

    private final TeammemberRepository teammemberRepository;

    public UsersController(TeammemberRepository teammemberRepository) {
        this.teammemberRepository = teammemberRepository;
    }

    @PostMapping("/users")
    @CrossOrigin
    public Teammember newUser(@Valid @RequestBody Teammember teammember) {
        teammember.key = UUID.randomUUID().toString();
        teammemberRepository.save(teammember);
        return teammember;
    }

    @GetMapping("/users")
    @CrossOrigin
    public Iterable<Teammember> findAll() {
        return teammemberRepository.findAll();
    }

    @PostMapping("/users/{key}")
    @CrossOrigin
    public ObjectNode updateImage(@PathVariable String key, @RequestParam("file") MultipartFile file) throws IOException {

        BufferedImage image = scale(file.getInputStream());
        String base64 = convertToBase64(image);

        Teammember teammember = teammemberRepository.findAllByKey(key);
        teammember.setPicture(base64);

        teammemberRepository.save(teammember);

        ObjectNode response = new ObjectMapper().createObjectNode();
        response.set("base64", new TextNode(base64));
        return response;
    }

    private String convertToBase64(BufferedImage img) throws IOException {
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(img, "PNG", Base64.getEncoder().wrap(os));
        return os.toString(UTF_8);
    }

    private BufferedImage scale(InputStream inputData) throws IOException {
        BufferedImage bf = ImageIO.read(inputData);
        return scale(bf);
    }

    private BufferedImage scale(BufferedImage bf) {
        double dWidthAndHeight = 250.;
        double fWidth = dWidthAndHeight / bf.getWidth();
        double fHeight = dWidthAndHeight / bf.getHeight();
        BufferedImage dbi = new BufferedImage((int) dWidthAndHeight, (int) dWidthAndHeight, BufferedImage.TYPE_INT_RGB);
        Graphics2D g = dbi.createGraphics();
        AffineTransform at = AffineTransform.getScaleInstance(fWidth, fHeight);
        g.drawRenderedImage(bf, at);
        return dbi;
    }


}
